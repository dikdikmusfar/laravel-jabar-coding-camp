<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


class CastController extends Controller
{
    public function index() {
        $cast = DB::table('casts')->get();
        return view('cast.index', compact('cast'));
    }

    public function create() {
        return view('cast.create');
    }

    public function store(Request $request) {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|numeric',
                'bio' => 'required'
            ],
            [
                'nama.required' => 'Nama ga boleh kosong loh bestie',
                'umur.required' => 'ya masa ga punya umur loh bestie',
                'bio.required' => 'deskripsiin diri kamu dulu yu'
            ]
        );

        $query = DB::table('casts')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');
    }

    public function show($id) {
        $cast = DB::table('casts')->where('id',$id)->first();
        return view('cast.show', compact('cast'));

    }

    public function edit($id) {
        $cast = DB::table('casts')->where('id',$id)->first();
        return view('cast.edit', compact('cast'));

    }

    public function update($id, Request $request) {
        $request->validate(
                [
                    'nama' => 'required',
                    'umur' => 'required|numeric',
                    'bio' => 'required'
                ],
                [
                    'nama.required' => 'Nama ga boleh kosong loh bestie',
                    'umur.required' => 'ya masa ga punya umur loh bestie',
                    'bio.required' => 'deskripsiin diri kamu dulu yu'
                ]
            );
        $query = DB::table('casts')-> where('id',$id)-> update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast');

    }

    public function destroy($id) {
        $query = DB::table('casts')->where('id',$id)->delete();
        return redirect('/cast');

    }
}

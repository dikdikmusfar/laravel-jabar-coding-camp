@extends('master')
@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Create a cast</h1>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>

      <!-- Main content -->
      <section class="content">

        <!-- Default box -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">create page</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <form action="/cast" method="POST">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="nama">Name</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Enter your Name">
              </div>
              @error('nama')
                  <div class="alert alert-danger">{{$message}}</div>
              @enderror
              <div class="form-group">
                <label for="umur">Age</label>
                <input type="number" class="form-control" id="umur" name="umur" placeholder="How old are you?">
              </div>
              @error('umur')
                  <div class="alert alert-danger">{{$message}}</div>
              @enderror
              <div class="form-group">
                <label for="bio">Bio</label>
                <textarea class="form-control" placeholder="Describe yourself" name="bio" id="bio" cols="30" rows="10"></textarea>
              </div>
              @error('bio')
                  <div class="alert alert-danger">{{$message}}</div>
              @enderror
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
          <!-- /.card-body -->
          <div class="card-footer">
            This is Create Form
          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <!-- /.content -->
@endsection

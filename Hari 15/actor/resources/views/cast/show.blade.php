@extends('master')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1>Detail Page </h1>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">This is data about {{$cast->nama}}</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
            </div>
        </div>
            <div class="card-body">
                <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch flex-column">
                    <div class="card bg-light d-flex flex-fill">
                      <div class="card-header text-muted border-bottom-0">
                        CAST
                      </div>
                      <div class="card-body pt-0">
                        <div class="row">
                          <div class="col-7">
                            <h2 class="lead"><b>{{$cast->nama}}</b></h2>
                            <p class="text-muted text-sm"><b>Age: </b> {{$cast->umur}} </p>
                            <p class="text-muted text-sm"><b>Bio: </b> {{$cast->bio}} </p>
                          </div>
                        </div>
                        <div class="row">
                            <a href="/cast/{{$cast->id}}/edit" class="btn btn-primary my-1">Edit</a>
                                <form action="/cast/{{$cast->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger my-1 ml-5" value="Delete">
                                </form>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        <!-- /.card-body -->
        <div class="card-footer">
            This is Detail Page
        </div>
        <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
@endsection

@extends('master')
@section('content')
    <section class="content-header">
        <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
            <h1>Jabar Coding Camp</h1>
            </div>
        </div>
        </div><!-- /.container-fluid -->
    </section>

<!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">create page</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
            </div>
        </div>
            <div class="card-body">
                <h1> Tugas Hari ke 15 </h1>
                <a href="/cast"><button class="btn btn-primary mb-2">Cast</button></a>
            </div>
        <!-- /.card-body -->
        <div class="card-footer">
            This is Home Page
        </div>
        <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
@endsection

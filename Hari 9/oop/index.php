<?php
    require('animal.php');
    require('ape.php');
    require('frog.php');
    
    $sheep = new Animal("shaun");

    echo "Name : $sheep->name  <br>"; 
    echo "legs : $sheep->legs <br>"; 
    echo "cold blooded : $sheep->cold_blooded <br>"; 

    $kodok = new Frog("buduk");
    echo "<br>name : $kodok->name  <br>"; 
    echo "legs : $kodok->legs <br>"; 
    echo "cold blooded : $kodok->cold_blooded <br>";
    $kodok->jump() ; 

    $sungokong = new Ape("kera sakti");
    echo "<br>name : $sungokong->name  <br>"; 
    echo "legs : $sungokong->legs <br>"; 
    echo "cold blooded : $sungokong->cold_blooded <br>";
    $sungokong->yell(); 
?>
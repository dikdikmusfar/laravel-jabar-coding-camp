<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Page</title>
</head>
<body>
    <h3>Buat Account Baru</h3>
    <h4>Sign Up Form</h4>
    <form action="/welcome">
        <label>First name : </label><br><br>
        <input type="text" name="first" id="first"><br><br>
        <label>Last name : </label><br><br>
        <input type="text" name="last" id="last"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender" id="male" value="1">Male <br>
        <input type="radio" name="gender" id="female" value="2">Female <br><br>
        <label>Nationality</label><br><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singapore">Singapore</option>
            <option value="Malaysia">Malaysia</option>
        </select> <br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="language" id="bind">Bahasa Indonesia <br>
        <input type="checkbox" name="language" id="bing">English <br>
        <input type="checkbox" name="language" id="other"> Other <br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>

</body>
</html>

<!-- Create Page -->

<!doctype html>

<html lang="en">

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <title>Create Data</title>
</head>

<body>
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>Create a Game</h1>
            </div>
          </div>
        </div>
    </section>

      <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">
                Game Form
            </h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <form action="/game" method="POST">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter your Game Name">
              </div>
              @error('name')
                  <div class="alert alert-danger">{{$message}}</div>
              @enderror
              <div class="form-group">
                <label for="developer">Developer</label>
                <input type="text" class="form-control" id="developer" name="developer" placeholder="The Developer">
              </div>
              @error('developer')
                  <div class="alert alert-danger">{{$message}}</div>
              @enderror
              <div class="form-group">
                <label for="year">Year</label>
                <input type="number" class="form-control" id="year" name="year" placeholder="which year">
              </div>
              @error('year')
                  <div class="alert alert-danger">{{$message}}</div>
              @enderror
              <div class="form-group">
                <label for="gameplay">Game Play</label>
                <textarea class="form-control" placeholder="Tell me about the game play" name="gameplay" id="gameplay" cols="30" rows="10"></textarea>
              </div>
              @error('gameplay')
                  <div class="alert alert-danger">{{$message}}</div>
              @enderror
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
          <!-- /.card-body -->
          <div class="card-footer">
            Sometime game can be a solution to heal someone.
          </div>
          <!-- /.card-footer-->
        </div>
        <!-- /.card -->

    </section>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameController extends Controller
{
    public function index() {
        $game = DB::table('games')->get();
        return view('game.index', compact('game'));
    }

    public function create() {
        return view('game.create');
    }

    public function store(Request $request) {
        $request->validate(
            [
                'name' => 'required',
                'gameplay' => 'required',
                'developer' => 'required',
                'year' => 'numeric'
            ],
            [
                'name.required' => 'The name field can not be empty',
                'gameplay.required' => 'how is the gameplay?',
                'developer.required' => 'who made this game?',
                'year.required' => 'which year?'
            ]
        );

        $query = DB::table('games')->insert([
            "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"]
        ]);
        return redirect('/game');
    }

    public function show($id) {
        $game = DB::table('games')->where('id',$id)->first();
        return view('game.show', compact('game'));

    }

    public function edit($id) {
        $game = DB::table('games')->where('id',$id)->first();
        return view('game.edit', compact('game'));

    }

    public function update($id, Request $request) {
        $request->validate(
                [
                    'name' => 'required',
                    'gameplay' => 'required',
                    'developer' => 'required',
                    'year' => 'numeric'
                ],
                [
                    'name.required' => 'The name field can not be empty',
                    'gameplay.required' => 'how is the gameplay?',
                    'developer.required' => 'who made this game?',
                    'year.required' => 'which year?'
                ]
            );
        $query = DB::table('games')-> where('id',$id)-> update([
            "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"]
        ]);
        return redirect('/game');

    }

    public function destroy($id) {
        $query = DB::table('games')->where('id',$id)->delete();
        return redirect('/game');

    }
}
